# pub_sub #
A simple tcp based publisher/subscriber.

## Basic ideas ##
 * Provide a simple interface for streaming data where the
 application has a straight forward way to provide a
 monotonically increasing key identifying each message.
 
 * IO is performed on background threads asynchronously.
 
 * Applications/users do not need to handle and manage
 individual connections.
 
 * This is modeled as a simple zmq like system.

 * This is written for possible use in streaming data in
 CDS.
 
 ## Implementation ##
 * C++14 or greater
 * Boost::asio for the networking (this is not exposed
 in the api)

## Examples and Notes ##
 A simple publisher is found in:
  * examples/publisher.cc
 
 A simple subscriber is found in:
  * examples/subscriber.cc
  
 The protocol is loosely described in:
  * pub_sub/private/pub_sub_intl.hh
 
 The main includes are:
  * pub_sub/include/pub_sub/common.hh
  * pub_sub/include/pub_sub/pub.hh
  * pub_sub/include/pub_sub/sub.hh
 
## License ##
GPL 3.  See COPYING.

The code under external/catch2 is licensed under the Boost license.
See the external/LICENSE_1_0.txt for the license. 
  
  