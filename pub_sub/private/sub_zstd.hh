//
// Created by jonathan.hanks on 1/27/21.
//

#ifndef DAQD_TRUNK_SUB_PLUGIN_ZLIB_HH
#define DAQD_TRUNK_SUB_PLUGIN_ZLIB_HH

#include <cds-pubsub/sub.hh>
#include "zstd_common.hh"

namespace pub_sub
{

    namespace detail
    {

        class ZStdSubscription : public pub_sub::plugins::Subscription
        {
        public:
            ZStdSubscription( pub_sub::SubDebugNotices& debug_hooks,
                              pub_sub::SubHandler       handler )
                : Subscription( ), inflate_( ), handler_{ std::move( handler ) }
            {
            }
            ~ZStdSubscription( ) override = default;

            void
            filter( pub_sub::SubMessage sub_message ) override
            {
                try
                {
                    handler_( pub_sub::SubMessage(
                        sub_message.sub_id( ),
                        sub_message.key( ),
                        inflate_( sub_message.message( ) ) ) );
                }
                catch ( std::runtime_error& )
                {
                    // inflate_ may throw on bad input, just ignore that
                }
            }

        private:
            InflateStream       inflate_;
            pub_sub::SubHandler handler_;
        };

    } // namespace detail

} // namespace pub_sub

#endif // DAQD_TRUNK_SUB_PLUGIN_ZLIB_HH
