//
// Created by jonathan.hanks on 1/29/21.
//

#include "zstd_common.hh"
#include <cds-pubsub/common.hh>
#include "catch.hpp"

#include <algorithm>
#include <set>

TEST_CASE( "Test deflate/inflate" )
{
    const int LENGTH = 10000;

    std::set< int > compression_levels;
    compression_levels.insert( ZSTD_CLEVEL_DEFAULT );
    for ( auto level = 1; level <= ZSTD_maxCLevel( ); ++level )
    {
        compression_levels.insert( level );
    }

    for ( const auto compression_level : compression_levels )
    {

        pub_sub::detail::DeflateStream deflater( compression_level );
        pub_sub::detail::InflateStream inflater;

        auto data =
            std::shared_ptr< unsigned char[] >( new unsigned char[ LENGTH ] );
        for ( auto i = 0; i < LENGTH; ++i )
        {
            data[ i ] = i % 256;
        }

        auto msg = pub_sub::Message( std::move( data ), LENGTH );
        auto compressed = deflater( msg );
        auto uncompressed = inflater( compressed );

        REQUIRE( msg.length == uncompressed.length );
        auto start_input = reinterpret_cast< unsigned char* >( msg.data( ) );
        auto start_output =
            reinterpret_cast< unsigned char* >( uncompressed.data( ) );
        REQUIRE(
            std::equal( start_input, start_input + LENGTH, start_output ) );
    }
}
