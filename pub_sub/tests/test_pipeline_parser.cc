//
// Created by jonathan.hanks on 2/1/21.
//

#include <catch.hpp>

#include "pipeline_parser.hh"
#include <iostream>
#include <string>
#include <vector>

TEST_CASE( "pipeline_parser" )
{
    struct Test_case
    {
        std::string                input;
        std::vector< std::string > expected;
    };
    std::vector< Test_case > test_cases{
        Test_case{ "", {} },
        Test_case{ "|", {} },
        Test_case{ "zlib(9)", { "zlib(9)" } },
        Test_case{ "zlib(0)", { "zlib(0)" } },
        Test_case{ "zlib", { "zlib" } },
        Test_case{ "zlib|", {} },
        Test_case{ "|zlib", {} },
        Test_case{ "tcp://localhost:9000", { "tcp://localhost:9000" } },
        Test_case{ "tcp://127.0.0.1:9000", { "tcp://127.0.0.1:9000" } },
        Test_case{ "tcp://127.0.0.1:9000|zlib",
                   { "tcp://127.0.0.1:9000", "zlib" } },
        Test_case{ "tcp://127.0.0.1:9000||zlib", {} },
        Test_case{ "tcp://127.0.0.1:9000|zlib|", {} },
        Test_case{ "tcp://127.0.0.1:9000|zlib(9)",
                   { "tcp://127.0.0.1:9000", "zlib(9)" } },
        Test_case{ "tcp://127.0.0.1:9000|zlib(0)",
                   { "tcp://127.0.0.1:9000", "zlib(0)" } },
        Test_case{ "tcp://127.0.0.1:9000|zlib(0)|other filter",
                   { "tcp://127.0.0.1:9000", "zlib(0)", "other filter" } },
        Test_case{
            "tcp://127.0.0.1:9000|zlib(0)|other filter|yet another filter",
            { "tcp://127.0.0.1:9000",
              "zlib(0)",
              "other filter",
              "yet another filter" } },
        Test_case{
            "tcp://127.0.0.1:9000|zlib(0)|other filter|yet another filter",
            { "tcp://127.0.0.1:9000",
              "zlib(0)",
              "other filter",
              "yet another filter" } },
    };

    for ( const auto& test_case : test_cases )
    {
        std::cout << test_case.input << "\n";
        REQUIRE( pub_sub::detail::parse_pipeline( test_case.input ) ==
                 test_case.expected );
    }
}
