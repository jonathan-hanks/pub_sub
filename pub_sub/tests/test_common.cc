#include <catch.hpp>

#include "pub_sub_intl.hh"

TEST_CASE( "Split_address helps to split addresses apart" )
{
    auto addr = pub_sub::detail::split_address( "127.0.0.1:9000" );
    REQUIRE( addr.first == "127.0.0.1" );
    REQUIRE( addr.second == 9000 );
}
