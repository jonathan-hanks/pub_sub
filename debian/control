Source: libcds-pubsub
Priority: optional
Maintainer: Jonathan Hanks <jonathan.hanks@ligo.org>
Build-Depends:
 debhelper (>= 11),
 devscripts,
 dpkg-dev,
 equivs,
 build-essential,
 cmake,
 libboost-dev,
 libboost-system-dev,
 python3-all-dev,
 python3-all,
 pybind11-dev,
 libzstd-dev,
Standards-Version: 4.1.3
Section: libs
Homepage: https://git.ligo.org/cds/pub_sub
#Vcs-Browser: https://salsa.debian.org/debian/libcds-pubsub
#Vcs-Git: https://salsa.debian.org/debian/libcds-pubsub.git

Package: libcds-pubsub-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libcds-pubsub (= ${binary:Version}), ${misc:Depends}
Description: A simple ip based publisher/subscriber system.
 A simple publisher/subscriber system build for the LIGO cds systems.
 .
 Provides facilities for doing:
  * tcp unicast
  * udp broadcast with support for retransmitting lost data
  * udp multicast with support for retransmitting lost data
 .
 This package contains the development files to use libcds-pubsub

Package: libcds-pubsub
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: A simple ip based publisher/subscriber system.
 A simple publisher/subscriber system build for the LIGO cds systems.
 .
 Provides facilities for doing:
  * tcp unicast
  * udp broadcast with support for retransmitting lost data
  * udp multicast with support for retransmitting lost data

Package: python3-cds-pubsub
Architecture: any
Multi-Arch: same
Depends: libcds-pubsub (= ${binary:Version}), ${misc:Depends}
Description: A python3 wrapper over the cds-pubsub library.